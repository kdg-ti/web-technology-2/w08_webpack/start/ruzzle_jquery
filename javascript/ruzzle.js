/**
 * Created with JetBrains WebStorm.
 * User: vochtenh & koray haha
 * Date: 8-5-13
 * Time: 16:08
 * Tpo change this template use File | Settings | File Templates.
 */
const GAME_TIME = 20;
const LETTERS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'o', 'u'];

let time;
let score;
let woord;
let gameStatus;
let timerId;
let dragging;


addEventListener("load", init, false);

function init(event) {
    gameStatus = "stopped";
    document.getElementById("btnStart").addEventListener("click", startGame, false);
    buttons = document.getElementById("ruzzlebord").getElementsByTagName("button");
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("mousedown", mouseDown, false);
        buttons[i].addEventListener("mouseup", mouseUp, false);
        buttons[i].addEventListener("mouseover", mouseOver, false);
    }
}

function startGame(event) {
    if (gameStatus == "running") return;
    dragging = false;
    time = GAME_TIME;
    document.getElementById("lblTime").innerHTML = "Time:" + time;
    score = 0;
    document.getElementById("lblScore").innerHTML = "Score:" + score;
    gameStatus = "running";
    woord = "";
    document.getElementById("lblWoord").innerHTML = woord;
    timerId = setInterval(updateTime, 1000);
    vulBordOp();
}

function vulBordOp() {
    buttons = document.getElementById("ruzzlebord").getElementsByTagName("button");
    for (let i = 0; i < buttons.length; i++) {
        const randomIndex = Math.floor(Math.random() * LETTERS.length);
        buttons[i].innerHTML = LETTERS[randomIndex].toUpperCase();
    }
}

function updateTime(event) {
    time--;
    document.getElementById("lblTime").innerHTML = "Time:" + time;
    if (time <= 0) {
        //stop the game...
        gameStatus = "stopped";
        clearInterval(timerId);
        alert("Game Finished! Your score: " + score);
        document.getElementById("lblWoord").innerHTML = `<em>klik "start" om te beginnen</em>`;
    }
}

function mouseDown(event) {
    if (gameStatus === "running") {
        dragging = true;
        woord += event.target.innerHTML;
        document.getElementById("lblWoord").innerHTML = woord;
    }
}

function mouseOver(event) {
    if (gameStatus === "running" && dragging) {
        woord += event.target.innerHTML;
        document.getElementById("lblWoord").innerHTML = woord;
    }
}

function mouseUp(event) {
    controleerWoord();
    dragging = false;
}

function controleerWoord() {
    if (woord.length > 1 && correcteWoorden.indexOf(woord.toLowerCase()) > -1) {
        score += woord.length;
        document.getElementById("lblScore").innerHTML = "Score:" + score;
    }
    woord = "";
}





